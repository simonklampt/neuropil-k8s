#!/usr/bin/env python
# SPDX-FileCopyrightText: 2016-2021 by pi-lar GmbH
# SPDX-License-Identifier: OSL-3.0

import os
import math
import json
import jinja2
from pprint import pprint
from datetime import datetime
from jinja2.environment import Template 
import pytz
from math import ceil
def kleinster_teil(a, b):
    return int(a) - (int(a) % b)

def max(a, b):
    if a< b:
        return a
    return b

def min(a, b):
    if a> b:
        return a
    return b

def generate_kubernetes_config(args, input_dir = "k8s-templates/", output_dir = "build/", **kwargs):
    pprint(args)
    templateLoader = jinja2.FileSystemLoader(searchpath="./")
    templateEnv = jinja2.Environment(loader=templateLoader)
    #templateEnv.globals.update(ggt=ggt)
    templateEnv.filters['kleinster_teil'] = kleinster_teil
    templateEnv.filters['max'] = max
    templateEnv.filters['min'] = min
    templateEnv.filters['ceil'] = ceil

    build_id = datetime.now(pytz.timezone("Europe/Berlin")).isoformat("T")
    
    for root, dirs, files in os.walk(input_dir):    
        for file in files:
            cleaned_root = root.replace(input_dir,"")
            output_file = os.path.join(output_dir, cleaned_root, file)

            input_file = os.path.join(root,file)
            
            template = templateEnv.get_template(input_file)
        
            render_config = {
                    "proto":"udp",
                    "labels":f', "build={build_id}_{cleaned_root}"',

                    "main_controller_count": args.main_controller,
                    "group_count":args.group_count,
                    "receiver_per_group":args.receiver_per_group,
                    "sender_per_group":args.sender_per_group,
                    "waves": args.waves,
                    "subjects": args.subjects,
                    "image_tag": os.getenv("IMAGE_TAG","main"),
                    "CI_PROJECT_NAMESPACE": os.getenv("CI_PROJECT_NAMESPACE","pi-lar")
            }
            try:
                with open(os.path.join(root,'config.json'), 'r') as f:
                    data = json.load(f)
                    for _config_key in render_config.keys():
                        if _config_key in data:
                            render_config[_config_key] = data[_config_key]
            except FileNotFoundError:
                pass
            os.makedirs(os.path.dirname(output_file), exist_ok=True)
            with open(output_file, "w+") as f:
                f.write(template.render(**render_config))


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser(description='Start a neuropil node/cluster')
    parser.add_argument('--main-controller', type=int, nargs='?', default=5,
                        help='How many neuropil main controller nodes should be started')

    parser.add_argument('--group_count', type=int, nargs='?', default=1,
                        help='How many neuropil support groups should be started')
    parser.add_argument('--receiver_per_group', type=int, nargs='?', default=1)
    parser.add_argument('--sender_per_group',   type=int, nargs='?', default=1)
    
    parser.add_argument('--waves', type=int, nargs='?', default=0,
                        help='In how many neuropil waves should the support groups be started')
    parser.add_argument('--subjects', type=int, nargs='?', default=0,
                        help='How many neuropil e2e groups should be started')
    args = parser.parse_args()
    if args.waves == 0:
        args.waves = args.group_count
    if args.subjects == 0:
        args.subjects = args.group_count
    
    generate_kubernetes_config(args)