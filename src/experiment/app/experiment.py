#!/usr/bin/env python3
import asyncio
from kubernetes import client, config
from pprint import pprint
from threading import Thread
from typing import Union, List
import requests
import json
from datetime import datetime, timedelta
import pytz
import os 
from random import randint
import re
import psutil


from neuropil import NeuropilNode, neuropil as np, NeuropilCluster, np_token, np_log_entry, np_message
process = psutil.Process(os.getpid())


loop = asyncio.get_event_loop()

config.load_incluster_config()
v1:client.CoreV1Api = client.CoreV1Api()
sleep_time:datetime=None
decrease_log_interval_at = None
desc =""
add_cluster_id_to_desc=False
cluster_counter=0
host = os.getenv("HOSTNAME","localhost")

ordinal_idx = int(host.split("-")[-1])

g_labels={
    'job':"neuropil-experiment", 
    "host":host
}


args = None

loki_url ="http://37.97.143.153:8080/loki/api/v1/push/"
class NeuropilRunnerNode(NeuropilNode):
    def __init__(self, **kwargs):
        self.next_join = datetime.now()
        self.joinStatus = False

        labels = g_labels
        global desc
        self.desc = desc
        self.cluster_counter =0
        global add_cluster_id_to_desc
        if add_cluster_id_to_desc:
            global cluster_counter
            self.cluster_counter = cluster_counter
            self.desc += f"-{cluster_counter:03}"
            cluster_counter += 1

        labels['desc'] = self.desc

        self.stream =  {
                    'stream': labels,
                    'values': []
        }
        self.entries = []
        self.connection_cache = {}
        self.receiving_data = 0
        self.sending_data = 0
        self.foundReceiver = False
        super().__init__(**kwargs)

        #self.use_identity(self.new_identity())
        
    def resolve_bootstrap_joins(self):
        try:
            current_namespace = open("/var/run/secrets/kubernetes.io/serviceaccount/namespace").read()
            ret = v1.list_namespaced_pod(current_namespace,watch=False, label_selector="app=neuropil-experiment")
            global args, g_labels
            for i in ret.items:
                if i.status.phase == "Running" and i.status.pod_ip and i.status.pod_ip != "None":
                    if i.metadata.labels['nptype'] == "main-controller" and not args.do_not_join_main_controllers:
                        yield f"*:udp4:{i.status.pod_ip}:30301"
        except Exception:
            print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} could not resolve join bootstrap nodes",flush=True)
            config.load_incluster_config()

    def resolve_node_joins(self):
        try:
            current_namespace = open("/var/run/secrets/kubernetes.io/serviceaccount/namespace").read()
            ret = v1.list_namespaced_pod(current_namespace,watch=False, label_selector="app=neuropil-experiment")
            global args, g_labels
            for i in ret.items:
                if i.status.phase == "Running" and i.status.pod_ip and i.status.pod_ip != "None":
                    if i.metadata.labels['nptype'] == "node-controller" and not args.do_not_join_node_controllers:
                        if f"e2e-{(int(i.metadata.name.split('-')[-1])%args.subjects):04d}" == g_labels["topic"]:
                            yield f"*:udp4:{i.status.pod_ip}:30302"
                        
        except Exception:
            print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} could not resolve join nodes",flush=True)
            config.load_incluster_config()
            
class NeuropilRunner(NeuropilCluster):

    @staticmethod
    def send_log_msg(node:NeuropilRunnerNode, msg:str, ts:float=None):
        ts_now = ts
        if not ts_now:
            ts_now = datetime.now(pytz.timezone('UTC')).timestamp()

        m = re.search('connection to node ([a-zA-Z0-9.-:]+).*', msg)
        if m:
            cache_key = m.group(1)
            node.connection_cache[cache_key] = [f"{ts_now*1000000000:.0f}",msg]
        else:
            node.entries.append([f"{ts_now*1000000000:.0f}", msg])                    


    def authentication_cb(self, node:NeuropilRunnerNode, token:np_token):
        authentication_result = self.authentication_count == 0 or node.get_route_count() < self.authentication_count
        s = f"{node.get_fingerprint()}: authn: {token.subject} -> {authentication_result}"
        #NeuropilRunner.send_log_msg(node, s)
        print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} {s}")
        return authentication_result

    def authorize_cb(self, node:NeuropilRunnerNode, token:np_token):
        s = f"{node.get_fingerprint()}: autho: {token.subject} -> {self.authorize_result}"
        #NeuropilRunner.send_log_msg(node, s)
        print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} {s}")
        return self.authorize_result

    async def np_log_write_handler(node:NeuropilRunnerNode, entry:np_log_entry):
        NeuropilRunner.send_log_msg(node, str(entry), ts=entry.timestamp)

    def np_log_write_callback (node:NeuropilRunnerNode, entry:np_log_entry):
        loop.create_task(NeuropilRunner.np_log_write_handler(node,entry))
        

    def msg_received_callback(self, node:NeuropilRunnerNode, message:np_message):
        #self.send_log_msg(f"{node.get_fingerprint()}: msg received from {message.__getattribute__('from')}  topic: {self.topic}")
        #self.send_log_msg(node, "receiving data")
        node.receiving_data +=1
        return True

    def __init__(self, cluster_count, port_range, proto, host, authentication_count:int, authorisation:bool,type:str,topic:str, **kwargs) -> NeuropilCluster:
        self._cluster_count = cluster_count
        print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} init",flush=True)
        log =   np.LOG_INFO + np.LOG_ERROR + np.LOG_WARNING  + np.LOG_DEBUG 
        log +=  np.LOG_EXPERIMENT#  + np.LOG_ROUTING #+ np.LOG_MESSAGE
        #log +=  np.LOG_JOBS 
        #log +=  np.LOG_KEYCACHE + np.LOG_NETWORK  
        super().__init__(cluster_count, port_range=port_range, proto=proto,host=host, auto_run=False, 
        log_write_fn=NeuropilRunner.np_log_write_callback,
        custom_node_class=NeuropilRunnerNode, log_level=log, n_threads=args.threads_per_node, **kwargs)
        
        now = datetime.now()

        self.next_log_send = now
        self.next_data_send = now
        self.send_system_data_at = now
        self.send_connection_data_at = now

        self.last_run = now
        self.join_counter=0
        
        self.authentication_count = authentication_count
        self.set_authenticate_cb(self.authentication_cb)

        self.authorize_result = authorisation
        self.set_authorize_cb(self.authorize_cb)
        self.topic = topic
        self.type = type
        if "receiver" in self.type:
            for node, mxp in self.get_mx_properties(self.topic):
                mxp.role = np.NP_MX_CONSUMER
                mxp.apply()
            self.set_receive_cb(self.topic, self.msg_received_callback)
        elif "sender" in self.type:
            for node, mxp in self.get_mx_properties(self.topic):
                mxp.apply()
        self.run(0)

    def send_to_loki(self):
        now = datetime.now()
        streams = []
        for node, status in self.get_status():
            if len(node.entries) > 0:
                node.stream['values'] += node.entries
                node.entries = []
                node.stream['values'].sort(key=lambda x: x[0], reverse=False)
                streams.append(node.stream)

        entries_count = sum([len(s['values']) for s in streams])
        
        if self.next_log_send < now or entries_count > 200:
            self.next_log_send = now + timedelta(seconds=10)
            if entries_count > 0:
                print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} data export: {entries_count}",end="",flush=True)
                try:
                    answer = requests.post(loki_url, json={'streams':streams}, headers={'Content-type': 'application/json'})
                    if answer.status_code < 200 or answer.status_code >= 300:
                        print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} error from loki: {answer.status_code}/{answer.text}",flush=True)
                        pprint(json.dumps({'streams':streams},indent=0))
                        print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} answer from loki: {answer.status_code}/{answer.text}",flush=True)
                        print(json.dumps(answer.json(),indent=0),flush=True)
                    else:
                        print(".",flush=True)
                except Exception as e:
                        print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} error from loki",flush=True)
                        pprint(json.dumps({'streams':streams},indent=2))
                        pprint(e)
                streams = []
                for node, status in self.get_status():
                    node.stream['values'] = []





    async def wait(self):
        global decrease_log_interval_at
        now = datetime.now()
        
        running = True
        idx = -1
        for node, status in self.get_status():
            node:NeuropilRunnerNode
            idx += 1
            if status == np.np_running:
                if sleep_time < now:
                    if now > node.next_join:
                        node.next_join = now + timedelta(seconds=min(60*30,self.join_counter*20+randint(0, 20)))
                        self.join_counter += 1
                        if not node.has_joined():
                            node.joinStatus = False
                            if type(args.join) is str:
                                print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} cluster-node-{idx} joining now '{args.join}'",flush=True)
                                node.join(args.join)
                            else:
                                for _j in args.join:
                                    print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} cluster-node-{idx} joining now '{_j}'",flush=True)
                                    node.join(_j)
                            for _j in node.resolve_node_joins():
                                print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} cluster-node-{idx} joining now '{_j}'",flush=True)
                                node.join(_j)
                        else:
                            if not node.joinStatus:
                                node.joinStatus = True
                                print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} cluster-node-{idx} joined",flush=True)

                        print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} cluster-node-{idx} (re-)joining bootsrappers:",flush=True)
                        for _j in node.resolve_bootstrap_joins():
                            print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} cluster-node-{idx} joining now '{_j}'",flush=True)
                            node.join(_j)
                        print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} cluster-node-{idx} (re-)joining node-controller:",flush=True)
                        for _j in node.resolve_node_joins():
                            print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} cluster-node-{idx} joining now '{_j}'",flush=True)
                            node.join(_j)
                    
                    if "sender" in self.type and self.next_data_send < now:
                        self.next_data_send = now + timedelta(seconds=randint(55,60))
                        if not node.foundReceiver:
                            print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} Check np_has_receiver_for",flush=True)
                            node.foundReceiver = node.np_has_receiver_for(self.topic)
                            print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} foundReceiver={node.foundReceiver}",flush=True)
                        if node.foundReceiver:
                            print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} Sending data {self.topic}",flush=True)
                            node.send(self.topic, "testdata")
                            node.sending_data += 1

            else:
                print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} {idx} cluster-node-errored. status: {status}",flush=True)
                running = False

            if now >= self.send_system_data_at:
                self.send_system_data_at = now + timedelta(seconds=1)
                line = f"system memory:{process.memory_info().rss} system cpu:{process.cpu_percent(interval=None)}"
                if now >= self.send_connection_data_at:
                    self.send_connection_data_at = now + timedelta(seconds=60)
                    # send connection cache
                    _connection_cache = node.connection_cache.values()
                    # append connection data entries
                    node.entries += _connection_cache
                    # prepare summary data
                    node_connections = len(node.connection_cache.values())
                    line += f" node_connections:{node_connections}"
                    # reset internal statistic accumulatoren
                    node.connection_cache = {}

                if self.topic:
                    if node.receiving_data > 0 or node.sending_data > 0:
                        line += f" receiving_data:{node.receiving_data} sending_data:{node.sending_data}"                        
                        # reset internal statistic accumulatoren
                        node.receiving_data = 0
                        node.sending_data = 0

                ts = f"{datetime.now(pytz.timezone('UTC')).timestamp()*1000000000:.0f}"
                node.entries.append([ts,line])

        self.send_to_loki()
        #self.run(0.1)
        asyncio.sleep(0.5)
        if running:
            self.last_run = now
            loop.create_task(self.wait())


    
def init():
    import argparse
    parser = argparse.ArgumentParser(description='Start a neuropil node/cluster')
    parser.add_argument('cluster_count', type=int, nargs='?', default=1,
                        help='How many neuropil nodes should be started')
    parser.add_argument('port_range', type=int, nargs='?', default=3000,
                        help='Start point of node range')
    parser.add_argument('protocol', type=str, nargs='?', default="udp4",choices=["udp4","tcp4","pas4","udp6","tcp6","pas6"],
                        help='Protocol of the node cluster')
    parser.add_argument('host', type=str, nargs='?', default=host,
                        help='Host of the node cluster')
    parser.add_argument('--authentication_count', type=int, nargs='?', default=0,
                        help='Maximum allowed routes')                               
    parser.add_argument('--data_throttle_after', type=int, nargs='?', default=10,
                        help='Minutes after start before the data to loki is throttled')
    parser.add_argument('--dns-name', type=str, nargs='?',
                        help='DNS of node')

    parser.add_argument('--type', type=str, nargs='?', default="controller", choices=["main-controller", "node-controller","node-receiver","node-sender"],
                        help='type of nodes')

    parser.add_argument('--waves', type=int, nargs='?', default=1, help='count of waves')
    parser.add_argument('--subjects', type=int, nargs='?', default=1, help='count of subjects')

    parser.add_argument('--join', type=str, nargs='*',default=[],
                        help='Add one or multiple join requests')

    parser.add_argument('--do_not_join_main_controllers', action='store_true',
                        help='Join main nodes')
    parser.add_argument('--do_not_join_node_controllers', action='store_true',
                        help='Join main nodes')

    parser.add_argument('--threads_per_node', type=int, nargs='?', default=8, help='count of threads per node')

    parser.add_argument('--labels',
                        metavar="KEY=VALUE",
                        nargs='*',
                        help="Add one or multiple labels to the loki stream "
                             "(do not put spaces before or after the = sign). "
                             "If a value contains spaces, you should define "
                             "it with double quotes: "
                             'foo="this is a sentence". Note that '
                             "values are always treated as strings."
                        )
    parser.add_argument('--never-authenticate', action='store_false',
                        help='Never authenticate')
    parser.add_argument('--always-authorize', action='store_true',
                        help='Always authorize')
    global args
    args = parser.parse_args()
    
    global g_labels
    if args.labels:
        for l in args.labels:
            k,v = l.split("=")
            g_labels[k]=f"{v}"
    
    g_labels["wave"] = f"{(ordinal_idx%args.waves):04d}"
    g_labels["topic"] = f"e2e-{(ordinal_idx%args.subjects):04d}"
    dns_name = args.dns_name if args.dns_name else args.host

    global desc
    desc = args.type
    if args.cluster_count > 1:
        global add_cluster_id_to_desc
        add_cluster_id_to_desc = True
        
    global sleep_time
    if args.waves > 0:
        sleep_min = int(ordinal_idx%args.waves) * 120

        sleep_time = datetime.now() + timedelta(seconds=randint(sleep_min, sleep_min+5))
        print(f"{datetime.now(pytz.timezone('Europe/Berlin')).isoformat('T')} sleep till: {sleep_time.strftime('%X')}sec",flush=True)        
    
    global decrease_log_interval_at
    decrease_log_interval_at = datetime.utcnow() + timedelta(minutes=args.data_throttle_after)
        
    runner = NeuropilRunner(
            args.cluster_count, args.port_range, args.protocol, host=args.host,
            authentication_count=args.authentication_count,
            authorisation=args.always_authorize,
            dns_name=dns_name,
            type=args.type,
            topic=g_labels["topic"]
    )
    pprint(args)

    loop.create_task(runner.wait())
    loop.run_forever()

if __name__ == "__main__":
    try:
        init()
    except Exception as e:
        f = open('/dev/termination-log', 'a+')
        f.write(f"ERROR in python script: {e}")
        f.close()
            
        raise e