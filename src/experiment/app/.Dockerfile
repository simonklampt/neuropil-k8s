#
# neuropil experiment Dockerfile
#
#

FROM registry.gitlab.com/pi-lar/neuropil/ci-enviroment:main

ARG CI_PROJECT_NAMESPACE=pi-lar
ARG NP_GIT_COMMIT=main

RUN git clone https://gitlab.com/$CI_PROJECT_NAMESPACE/neuropil.git

WORKDIR /neuropil

RUN git checkout "$NP_GIT_COMMIT" && \
    echo "building upon git ref $NP_GIT_COMMIT from https://gitlab.com/$CI_PROJECT_NAMESPACE/neuropil.git"

RUN apt-get update \
 && apt-get -y install curl python3-pip libsodium23
RUN ./do ensure_dependencies && \
    ./do build --RELEASE && \
    ./do build --RELEASE python

RUN rsync -a  build/neuropil/bin/* /usr/local/bin && \
    rsync -al build/neuropil/lib/* /usr/local/lib && \
    rsync -a  include/neuropil* /usr/local/include/

RUN ls /usr/local/bin/

RUN ldconfig && \
    pip3 install build/bindings/python/dist/neuropil-*.zip &&\
    pip3 install kubernetes pytz psutil requests

WORKDIR /

RUN rm -r /neuropil

COPY experiment.py experiment.py

CMD [""]
